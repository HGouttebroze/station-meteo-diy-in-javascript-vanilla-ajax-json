# Hugues GOUTTEBROZE

# EXERCICE JavaScript Vanilla TP RNCP (niveau 6) CDA Human Booster 2021

Dans cet exercice, nous allons réaliser un appel AJAX en pur JavaScript Vanilla pour contacter une API nous permettant de récupérer les données météorologique en fonction de la localisation envoyée.

## Énoncé

Une structure de base (HTML, CSS) est fournie.

Cet exercice va permettre de faire un appel vers une API externe, de lui envoyer des données et d'en recevoir, puis de générer l'affichage adéquat.

L'utilisateur doit saisir la version anglaise du nom de la ville.

Cet exercice contient un appel AJAX. Nous allons donc devoir utiliser un serveur local.
**accéder au projet via le Locahost bien entendu, (si utilisation de l'IDE VS Codium, ne surtout pas utiliser l'extention LiveServer, ni en ouvrant directement le fichier HTML)**

## Debugage

- Utiliser les `console.log` pour valider le bon fonctionnement des différentes étapes du code
- Créer des breakpoints via l'IDE, ou dans l'inpecteur.

### API utilisée

- [Documentation OpenWeatherMap](https://openweathermap.org/current)
- Utilisation gratuite ds la limite de 1000 requêtes/jour.
