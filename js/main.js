"use strict"; // Mode strict du JavaScript

//import * as storage from "./storage.js";

// /*************************************************************************************************/
// /* ***************************************** FONCTIONS ***************************************** */
// /*************************************************************************************************/

/**
 * fonction search appelé au click sur le btn
 */
function getMeteo() {
  // je recupère la valeur de la ville saisie
  let city = document.querySelector("#ville").value.trim().replace(/\s/g, ""); // methode trim supprime les espace avant et après de la valeur saisi // à l'inrérieur, on utilice la méthode replace, 1 regex
  console.log(city);

  // constitution de l'url

  let url = "http://api.openweathermap.org/data/2.5/weather?q=" + city;
  url += "&appid=6fe449302eecca061d42664da250b16c";
  url += "&units=metric";
  url += "&lang=fr";

  fetch(url)
    .then((response) => response.json())
    .then((datas) => displayMeteo(datas));
  console.log(url);
}

function displayMeteo(datas) {
  document.querySelector("h2 strong").textContent = datas.name;
  document.querySelector("h2 sup").textContent = datas.sys.country;
  document.querySelector("p strong").textContent = datas.main.temp;
  document.querySelector("small").textContent = datas.weather[0].description;
  document.querySelector(
    "img"
  ).src = `https://openweathermap.org/img/wn/${datas.weather[0].icon}.png`;
  document.querySelector("article").classList.remove("hide");
  console.log(datas);
}
console.log("ok");

// /*************************************************************************************************/
// /* ************************************** CODE PRINCIPAL *************************************** */
// /*************************************************************************************************/

/*
 * Installation d'un gestionnaire d'évènement déclenché quand l'arbre DOM sera
 * totalement construit par le navigateur.
 *
 * Le gestionnaire d'évènement est une fonction anonyme que l'on donne en deuxième
 * argument directement à document.addEventListener().
 */
document.addEventListener("DOMContentLoaded", function () {
  // Installation des gestionnaires d'évènement.
  // gestionnaire d'événement sur le bouton `#submit` qui appelle une fonction `search()`
  document.querySelector("#submit").addEventListener("click", getMeteo);
});
