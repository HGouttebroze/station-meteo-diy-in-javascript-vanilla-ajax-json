Afin de bien aborder l'exercice, voici les étapes que nous vous recommandons de respecter :

1. Mettre un gestionnaire d'événement sur le bouton `#submit` qui appelle une fonction `search()`
2. Coder la fonction `search()`

   - Récupérer la valeur de la ville saisie → il ne doit pas y avoir d'espaces vides dans la chaîne de caractères !
   - constituer l'URL pour contacter l'API :

     - la ville
     - la clé API (créer un compte pour obtenir votre API key → attention, le nombre de requête par heure est limité 🤔)
     - l'unité en degrés
     - la langue en FR
     - ...

     Vous pouvez avoir plus de détails en consultant [la documentation](https://openweathermap.org/current)

   - réaliser l'appel AJAX qui s'attend à recevoir du JSON
   - en cas de success, appeler une fonction qui injecte les données retournées dans l'article caché, puis l'affiche
     → pour l'icone : [https://openweathermap.org/weather-conditions](https://openweathermap.org/weather-conditions)

api.openweathermap.org/data/2.5/weather?q={city name},{units metric},{lang}&appid={API key}

api.openweathermap.org/data/2.5/find?q=London&units=metric

openweather: 6fe449302eecca061d42664da250b16c

api.openweathermap.org/data/2.5/weather?q={city name},{units metric},{lang}&appid=6fe449302eecca061d42664da250b16c
